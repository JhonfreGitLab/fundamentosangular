import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {
  title = 'platzi-store';

  names = ['Nicolas','Jhon','Daniel','Fernando'];

  power = 10;
  agregarName = false;
  nombre = '';
  mensajeError = '';

  constructor() { }

  ngOnInit(): void {
  }

  addName(){
    this.agregarName = true
  }

  saveName(){
    if(this.nombre!=''){
      this.names.push(this.nombre);
      this.nombre = '';
      this.agregarName = false;
      this.mensajeError = '';
    }else{
      this.mensajeError = 'Por favor digite un valor correcto';
    }
  }

  deleteName(index:number){
    this.names.splice(index,1);
  }
}
